<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>A Simple Form</h1>

    <fieldset>
        <legend>Form with possible elements</legend>
        <form action="form_submitted.php" method="post" enctype="multipart/form-data">
            <ul>
                <li>
                <label>Enter Your Name</label>
                <input type="text" id="fullname" name="fullname" placeholder="Name"> <br>
                </li>
                <li>
                <label>Email</label>
                <input type="email" id="email" name="email" placeholder="Email"> <br>
                </li>
                <li>
                <label>Password</label>
                <input type="password" id="pwd" name="pwd" placeholder="password"> <br>
                </li> 
            </ul>     

            <legend>Hobby ?</legend>
            <ul>
                <li>
                <label>Gamming</label>
                <input type="checkbox" id="hb" name="hb[]" value="gaming" checked> <br>
                </li>
                <li>
                <label>Coding</label>
                <input type="checkbox" id="hb" name="hb[]" value="coding"> <br>
                </li>
                <li>
                <label>Movies</label>
                <input type="checkbox" id="hb" name="hb[]" value="movies"> <br>
                </li>
            </ul> 
            
        <legend>Are you want to share your information ?</legend>
            <ul>
                <li>
                <label>Yes</label>
                <input type="radio" id="agree" name="agree" value="yes" checked> <br>
                </li>
                <li>
                <label>No</label>
                <input type="radio" id="agree" name="agree" value="no"> <br>
                </li>
            </ul>     

            
            <label>Tell about yourself</label>
            <br>
            <textarea name="message" placeholder="write something about yourself"></textarea> <br>

            <label>Profession</label>
            <select id="pro" name="pro">
                    <option value="not selected">Not Selected</option>
                    <option value="student">Student</option>
                    <option value="engineer">Engineer</option>
                    <option value="doctor">Doctor</option>
            </select>

            <br>

            <label>Upload Your Picture</label>
            <input type="file" id="pic" name="pic"> <br>

            <button type="Submit">Save</button>
        </form>
       
    </fieldset>    

</body>
</html>