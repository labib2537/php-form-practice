<?php

print_r($_POST);
echo "<br>";
print_r($_FILES);
echo "<br>";
//var_dump($_POST);

$fn = (array_key_exists("fullname",$_POST))?$_POST["fullname"]:"";
$em = (array_key_exists("email",$_POST))?$_POST["email"]:"";
$ps = (array_key_exists("pwd",$_POST))?$_POST["pwd"]:"";

$hbs = $_POST["hb"]??array();

//var_dump($hbs);

$tc = (array_key_exists("agree",$_POST))?$_POST["agree"]:"";
$msg = (array_key_exists("message",$_POST))?$_POST["message"]:"";
$pr = (array_key_exists("pro",$_POST))?$_POST["pro"]:"";

//$img = $_POST["pic"];



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submitted Data</title>
</head>
<body>

<h1> Submitted Data </h1>

<dl>
    <dt>Full Name</dt>
    <dd><?=$fn;?></dd>
    <dt>Email</dt>
    <dd><?=$em;?></dd>
    <dt>Password</dt>
    <dd><?=$ps;?></dd>

    <?php if(count($hbs) > 0): ?>

    <dt>Hobby</dt> 
    <dd>
        <ul>
            <?php foreach($hbs as $hb):?>
            <li><?php echo $hb; ?></li>
            <?php endforeach; ?>
            
        </ul>
    </dd>

    <?php endif; ?>
    
    <dt>Terms</dt>
    <dd>
        <?php 
        if($tc=='yes') echo "I want to share my information.";
        else echo "I don't want to share my information"; 
        ?>
    </dd>
    <dt>About</dt>
    <dd><?=$msg;?></dd>
    <dt>Profession</dt>
    <dd><?=$pr;?></dd>
    
</dl>

    
</body>
</html>